    // src/components/contacts.js

    import React from 'react'

    const Contacts = ({ contacts }) =>

       {
       contacts.sort(function(a, b){
       if(a.name.toLowerCase() < b.name.toLowerCase()) return -1;
       if(a.name.toLowerCase() > b.name.toLowerCase()) return 1;
      return 0;})
        return  (
        <div>
          <center><h1>Contact List</h1></center>
          {
            contacts.map((contact) => (
            <div class="card">
              <div class="card-body">
                <h5 class="card-title">{contact.name}</h5>
                <h6 class="card-subtitle mb-2 text-muted">{contact.email}$
                <p class="card-text">{contact.company.name}</p>
              </div>
            </div>
          ))}
        </div>
      )
    };

    export default Contacts
